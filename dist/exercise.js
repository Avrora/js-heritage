/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/rpg.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/exo/character.js":
/*!******************************!*\
  !*** ./src/exo/character.js ***!
  \******************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var _item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item */ "./src/exo/item.js");
/*export class Character {
        /**
    * @param {number} health
    * @param {string} name
    * @param {number} armor
    * @param {number} exp
    * @param {Array<Item>} inventory //Ili Item[]
    */
/*    constructor(health, name, armor, exp, inventory = []) { //V inventorii nyzno dobavit skobki [], inache kod ne srabotaet
        this.health = health;
        this.name = name;
        this.armor = armor;
        this.exp = exp;
        this.inventory = inventory;
    }

    /**
     * @param {Item} item
     */
/*    takeItem(item) {
        this.inventory .push(item); //Tochka pered push, potomy chto eto funkziya
    }
}
*/


class Character {
    /**
     * 
     * @param {number} health 
     * @param {string} name 
     * @param {number} armor 
     * @param {number} exp 
     */
    constructor(health, name, armor, exp) {
        this.health = health;
        this.name = name;
        this.armor = armor;
        this.exp = exp;
        this.inventory = [];
        this.weaponInventory = [];
    }
    /**
     * La méthode takeItem permet à un personnage de "ramasser" un item
     * et de le mettre dans son sac (son inventory)
     * @param {Item} item 
     */
    takeItem(item) {
        //On ne met dans l'inventaire que les instance d'Item et rien d'autre
        if (item instanceof _item__WEBPACK_IMPORTED_MODULE_0__["Item"]) {
            this.inventory.push(item);
        } else {
            console.log("I can only pick items!");
        }

    }
    /**
     * Cette méthode fera qu'un personnage prendra un Item dans son
     * sac puis utilisera l'item sur lui même.
     * @param {number} index l'index de l'item à utiliser
     */
    useItem(index) {
        //On récupère l'item qui est dans l'inventaire à l'index voulu
        let choosedItem = this.inventory[index];
        //On utilise la méthode useOn de l'item en donnant l'instance
        //actuelle de personnage comme cible de l'item
        choosedItem.useOn(this);
    }



    //////////////////////// NACHALA ZDES!!!!
    showInventory() { //kak (this.inventory) 
        /*console.log(character1.inventory);*/
        for (let index=0; index<=this.inventory.length-1; index++) {
            console.log(this.inventory[index].name);
        }
    }

    showOneItem(item){
        
    }

    takeItems(items) { //Eto budet mtablitsa pradmetov array[Item];
        for(let index=0; index<=items.length-1; index++)  {// Zadaem znachenie items - kak let items[] (no tak nelzya s massivom);
            let item = items[index];
            this.inventory.push(item); //ili this.takeItem(item);

        }    
    }

    eatFruits() {
        //fork, pear, banana, apple = inventory = items

       /* console.log(this.health);

        for(let index = 0; index<=this.inventory.length-1; index++) { //Otsuda berem frykti
            if(this.inventory[index].name.indexOf("Fruit")) { //Proverka frykt li eto (vmesto indexOf mojno inludes)
                this.health += 10;
                console.log(this.health);
            }
            else {
                console.log("C'est pas un fruit")
            }
        } */

    //PROVERKI 
        //Otsuda berem frykti
        for(let index = 0; index<this.inventory.length; index++) {

        //Proverka skolko veshei v inventare
        //console.log(this.inventory.length);

            //Proverka frykt li eto (vmesto indexOf mojno inludes)
            if(this.inventory[index].name.includes("Fruit")) {
                console.log(this.inventory[index].name + ". Ok, it's a fruit");
                this.health +=10;
                console.log(this.health);
            } else {
                console.log(this.inventory[index].name +" isn't a fruit");
            }
        }
    }

    eatOneFruit(item){

    }

    //Vnachale nyzno dobavit weaponInventory v parrametri personaza (v samom verhy faila)
    storeInventory() {
        for(let index=0; index<=this.inventory[index]; index++) {
            if(this.inventory[index].name.includes("Weapon")) {
                console.log("Ok, it's a weapon");
                this.weaponInventory.push(this.inventory[index]);

            }
        }
    }
    //Gde nahoditsya oryzie (this.inventory, weaponInventory)
    //Osmatrivaem oryzie
    //Proveryaem, oryzie li eto
    //Berem oryzie i dobavlyaem ego v weaponInventory
    //Ydalyaem oryzie iz this.inventory
}

/***/ }),

/***/ "./src/exo/item.js":
/*!*************************!*\
  !*** ./src/exo/item.js ***!
  \*************************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./character */ "./src/exo/character.js");
/*import { Character } from "./character";

export class Item {
    /**
    * @param {string} name
    */
/*   constructor(name) {
    this.name = name;
   }

    /**
    * @param {Character} character
    */
/*    useItem(character) {
        console.log ("Using " + item.name + " on " + character.name)
    }
}*/



class Item{
    /**
     * @param {String} name 
     */
    constructor(name){
        this.name = name;

    }
    /**
     * Méthode qui utilise l'item sur un personnage donné
     * @param {Character} character Le personnage sur lequel on utilise l'item
     */
    useOn(character){
        /* Ici, on fait une concaténation en utilisant à la fois le
        name de l'item qu'on utilise (this.name) et en même temps le name
        du personnage sur lequel on utilise l'item (character.name)
         */
        console.log("Using " + this.name + " on " + character.name);
        
    }       
}



/***/ }),

/***/ "./src/rpg.js":
/*!********************!*\
  !*** ./src/rpg.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _exo_character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./exo/character */ "./src/exo/character.js");
/* harmony import */ var _exo_item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./exo/item */ "./src/exo/item.js");
/*
1. Dans le src, créer un nouveau dossier exo, dans ce dossier exo, 
créer un fichier character.js avec une classe Character à l'intérieur
2. Faire la méthode takeItem qui va juste prendre l'argument item et l'ajouter dans l'inventaire du personnage 
(ajouter un truc dans un tableau....)
(on peut laisser le useItem de côté pour le moment, vu qu'on a pas encore fait la classe Item)
3. Dans le dossier exo, créer une nouvelle classe Item. La méthode useOn pour le moment va juste 
faire un console log disant que "Using [nom de l'item] on [nom du personnage]"
4. Faire la méthode useItem du Character qui va en gros prendre l'item à l'index donné dans 
sa propriété inventaire puis déclencher la méthode useOn de l'Item sur le Character lui même
5. Modifier la méthode takeItem pour faire en sorte que le personnage ne puisse prendre que des élément étant 
des instances de item 
6. Rajouter un if avec un instanceof pour vérifier si l'argument item est bien une instance de Item avant de faire le push
*/





let romy = new _exo_character__WEBPACK_IMPORTED_MODULE_0__["Character"](100, 'Romy', 0, 0);

let fork = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"]("Fork");
let pear = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"]("Fruit: pear");
let banana = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"]("Fruit: banana");
let apple = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"]("Fruit: apple");
let orange = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"]("Fruit: orange");
let sword = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"] ("Weapon: sword");

romy.takeItem(fork);
romy.takeItem(pear);
romy.takeItem(banana);
romy.takeItem(apple);
romy.takeItem(orange);
romy.takeItem(sword);

romy.useItem(0);

romy.showInventory();

romy.eatFruits();



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2V4by9jaGFyYWN0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2V4by9pdGVtLmpzIiwid2VicGFjazovLy8uL3NyYy9ycGcuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsY0FBYyxPQUFPO0FBQ3JCLGNBQWMsT0FBTztBQUNyQixjQUFjLE9BQU87QUFDckIsY0FBYyxPQUFPO0FBQ3JCLGNBQWMsWUFBWTtBQUMxQjtBQUNBLDZEQUE2RDtBQUM3RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlLEtBQUs7QUFDcEI7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDOEI7O0FBRXZCO0FBQ1A7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLEtBQUs7QUFDcEI7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLDBDQUFJO0FBQ2hDO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0EscUJBQXFCO0FBQ3JCLDRDQUE0QztBQUM1Qyx5QkFBeUIsZ0NBQWdDO0FBQ3pEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxzQkFBc0I7QUFDdEIsd0JBQXdCLHVCQUF1QixZQUFZO0FBQzNEO0FBQ0Esc0NBQXNDOztBQUV0QyxTO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSwwQkFBMEIsZ0NBQWdDLFdBQVc7QUFDckUsNkRBQTZEO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBLDBCQUEwQiw2QkFBNkI7O0FBRXZEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0Esd0JBQXdCLDhCQUE4QjtBQUN0RDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQy9JQTtBQUFBO0FBQUE7QUFBQSxVQUFVLFlBQVk7O0FBRXRCO0FBQ0E7QUFDQSxjQUFjLE9BQU87QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxjQUFjLFVBQVU7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUV1Qzs7QUFFakM7QUFDUDtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxVQUFVO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUN4Q0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFNEM7QUFDVjs7O0FBR2xDLGVBQWUsd0RBQVM7O0FBRXhCLGVBQWUsOENBQUk7QUFDbkIsZUFBZSw4Q0FBSTtBQUNuQixpQkFBaUIsOENBQUk7QUFDckIsZ0JBQWdCLDhDQUFJO0FBQ3BCLGlCQUFpQiw4Q0FBSTtBQUNyQixnQkFBZ0IsOENBQUk7O0FBRXBCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSIsImZpbGUiOiJleGVyY2lzZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL3JwZy5qc1wiKTtcbiIsIi8qZXhwb3J0IGNsYXNzIENoYXJhY3RlciB7XG4gICAgICAgIC8qKlxuICAgICogQHBhcmFtIHtudW1iZXJ9IGhlYWx0aFxuICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAgICAqIEBwYXJhbSB7bnVtYmVyfSBhcm1vclxuICAgICogQHBhcmFtIHtudW1iZXJ9IGV4cFxuICAgICogQHBhcmFtIHtBcnJheTxJdGVtPn0gaW52ZW50b3J5IC8vSWxpIEl0ZW1bXVxuICAgICovXG4vKiAgICBjb25zdHJ1Y3RvcihoZWFsdGgsIG5hbWUsIGFybW9yLCBleHAsIGludmVudG9yeSA9IFtdKSB7IC8vViBpbnZlbnRvcmlpIG55em5vIGRvYmF2aXQgc2tvYmtpIFtdLCBpbmFjaGUga29kIG5lIHNyYWJvdGFldFxuICAgICAgICB0aGlzLmhlYWx0aCA9IGhlYWx0aDtcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5hcm1vciA9IGFybW9yO1xuICAgICAgICB0aGlzLmV4cCA9IGV4cDtcbiAgICAgICAgdGhpcy5pbnZlbnRvcnkgPSBpbnZlbnRvcnk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtJdGVtfSBpdGVtXG4gICAgICovXG4vKiAgICB0YWtlSXRlbShpdGVtKSB7XG4gICAgICAgIHRoaXMuaW52ZW50b3J5IC5wdXNoKGl0ZW0pOyAvL1RvY2hrYSBwZXJlZCBwdXNoLCBwb3RvbXkgY2h0byBldG8gZnVua3ppeWFcbiAgICB9XG59XG4qL1xuaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuL2l0ZW1cIjtcblxuZXhwb3J0IGNsYXNzIENoYXJhY3RlciB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGhlYWx0aCBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gYXJtb3IgXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGV4cCBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihoZWFsdGgsIG5hbWUsIGFybW9yLCBleHApIHtcbiAgICAgICAgdGhpcy5oZWFsdGggPSBoZWFsdGg7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuYXJtb3IgPSBhcm1vcjtcbiAgICAgICAgdGhpcy5leHAgPSBleHA7XG4gICAgICAgIHRoaXMuaW52ZW50b3J5ID0gW107XG4gICAgICAgIHRoaXMud2VhcG9uSW52ZW50b3J5ID0gW107XG4gICAgfVxuICAgIC8qKlxuICAgICAqIExhIG3DqXRob2RlIHRha2VJdGVtIHBlcm1ldCDDoCB1biBwZXJzb25uYWdlIGRlIFwicmFtYXNzZXJcIiB1biBpdGVtXG4gICAgICogZXQgZGUgbGUgbWV0dHJlIGRhbnMgc29uIHNhYyAoc29uIGludmVudG9yeSlcbiAgICAgKiBAcGFyYW0ge0l0ZW19IGl0ZW0gXG4gICAgICovXG4gICAgdGFrZUl0ZW0oaXRlbSkge1xuICAgICAgICAvL09uIG5lIG1ldCBkYW5zIGwnaW52ZW50YWlyZSBxdWUgbGVzIGluc3RhbmNlIGQnSXRlbSBldCByaWVuIGQnYXV0cmVcbiAgICAgICAgaWYgKGl0ZW0gaW5zdGFuY2VvZiBJdGVtKSB7XG4gICAgICAgICAgICB0aGlzLmludmVudG9yeS5wdXNoKGl0ZW0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJIGNhbiBvbmx5IHBpY2sgaXRlbXMhXCIpO1xuICAgICAgICB9XG5cbiAgICB9XG4gICAgLyoqXG4gICAgICogQ2V0dGUgbcOpdGhvZGUgZmVyYSBxdSd1biBwZXJzb25uYWdlIHByZW5kcmEgdW4gSXRlbSBkYW5zIHNvblxuICAgICAqIHNhYyBwdWlzIHV0aWxpc2VyYSBsJ2l0ZW0gc3VyIGx1aSBtw6ptZS5cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gaW5kZXggbCdpbmRleCBkZSBsJ2l0ZW0gw6AgdXRpbGlzZXJcbiAgICAgKi9cbiAgICB1c2VJdGVtKGluZGV4KSB7XG4gICAgICAgIC8vT24gcsOpY3Vww6hyZSBsJ2l0ZW0gcXVpIGVzdCBkYW5zIGwnaW52ZW50YWlyZSDDoCBsJ2luZGV4IHZvdWx1XG4gICAgICAgIGxldCBjaG9vc2VkSXRlbSA9IHRoaXMuaW52ZW50b3J5W2luZGV4XTtcbiAgICAgICAgLy9PbiB1dGlsaXNlIGxhIG3DqXRob2RlIHVzZU9uIGRlIGwnaXRlbSBlbiBkb25uYW50IGwnaW5zdGFuY2VcbiAgICAgICAgLy9hY3R1ZWxsZSBkZSBwZXJzb25uYWdlIGNvbW1lIGNpYmxlIGRlIGwnaXRlbVxuICAgICAgICBjaG9vc2VkSXRlbS51c2VPbih0aGlzKTtcbiAgICB9XG5cblxuXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vIE5BQ0hBTEEgWkRFUyEhISFcbiAgICBzaG93SW52ZW50b3J5KCkgeyAvL2thayAodGhpcy5pbnZlbnRvcnkpIFxuICAgICAgICAvKmNvbnNvbGUubG9nKGNoYXJhY3RlcjEuaW52ZW50b3J5KTsqL1xuICAgICAgICBmb3IgKGxldCBpbmRleD0wOyBpbmRleDw9dGhpcy5pbnZlbnRvcnkubGVuZ3RoLTE7IGluZGV4KyspIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuaW52ZW50b3J5W2luZGV4XS5uYW1lKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNob3dPbmVJdGVtKGl0ZW0pe1xuICAgICAgICBcbiAgICB9XG5cbiAgICB0YWtlSXRlbXMoaXRlbXMpIHsgLy9FdG8gYnVkZXQgbXRhYmxpdHNhIHByYWRtZXRvdiBhcnJheVtJdGVtXTtcbiAgICAgICAgZm9yKGxldCBpbmRleD0wOyBpbmRleDw9aXRlbXMubGVuZ3RoLTE7IGluZGV4KyspICB7Ly8gWmFkYWVtIHpuYWNoZW5pZSBpdGVtcyAtIGthayBsZXQgaXRlbXNbXSAobm8gdGFrIG5lbHp5YSBzIG1hc3Npdm9tKTtcbiAgICAgICAgICAgIGxldCBpdGVtID0gaXRlbXNbaW5kZXhdO1xuICAgICAgICAgICAgdGhpcy5pbnZlbnRvcnkucHVzaChpdGVtKTsgLy9pbGkgdGhpcy50YWtlSXRlbShpdGVtKTtcblxuICAgICAgICB9ICAgIFxuICAgIH1cblxuICAgIGVhdEZydWl0cygpIHtcbiAgICAgICAgLy9mb3JrLCBwZWFyLCBiYW5hbmEsIGFwcGxlID0gaW52ZW50b3J5ID0gaXRlbXNcblxuICAgICAgIC8qIGNvbnNvbGUubG9nKHRoaXMuaGVhbHRoKTtcblxuICAgICAgICBmb3IobGV0IGluZGV4ID0gMDsgaW5kZXg8PXRoaXMuaW52ZW50b3J5Lmxlbmd0aC0xOyBpbmRleCsrKSB7IC8vT3RzdWRhIGJlcmVtIGZyeWt0aVxuICAgICAgICAgICAgaWYodGhpcy5pbnZlbnRvcnlbaW5kZXhdLm5hbWUuaW5kZXhPZihcIkZydWl0XCIpKSB7IC8vUHJvdmVya2EgZnJ5a3QgbGkgZXRvICh2bWVzdG8gaW5kZXhPZiBtb2pubyBpbmx1ZGVzKVxuICAgICAgICAgICAgICAgIHRoaXMuaGVhbHRoICs9IDEwO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuaGVhbHRoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQydlc3QgcGFzIHVuIGZydWl0XCIpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0gKi9cblxuICAgIC8vUFJPVkVSS0kgXG4gICAgICAgIC8vT3RzdWRhIGJlcmVtIGZyeWt0aVxuICAgICAgICBmb3IobGV0IGluZGV4ID0gMDsgaW5kZXg8dGhpcy5pbnZlbnRvcnkubGVuZ3RoOyBpbmRleCsrKSB7XG5cbiAgICAgICAgLy9Qcm92ZXJrYSBza29sa28gdmVzaGVpIHYgaW52ZW50YXJlXG4gICAgICAgIC8vY29uc29sZS5sb2codGhpcy5pbnZlbnRvcnkubGVuZ3RoKTtcblxuICAgICAgICAgICAgLy9Qcm92ZXJrYSBmcnlrdCBsaSBldG8gKHZtZXN0byBpbmRleE9mIG1vam5vIGlubHVkZXMpXG4gICAgICAgICAgICBpZih0aGlzLmludmVudG9yeVtpbmRleF0ubmFtZS5pbmNsdWRlcyhcIkZydWl0XCIpKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5pbnZlbnRvcnlbaW5kZXhdLm5hbWUgKyBcIi4gT2ssIGl0J3MgYSBmcnVpdFwiKTtcbiAgICAgICAgICAgICAgICB0aGlzLmhlYWx0aCArPTEwO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuaGVhbHRoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5pbnZlbnRvcnlbaW5kZXhdLm5hbWUgK1wiIGlzbid0IGEgZnJ1aXRcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBlYXRPbmVGcnVpdChpdGVtKXtcblxuICAgIH1cblxuICAgIC8vVm5hY2hhbGUgbnl6bm8gZG9iYXZpdCB3ZWFwb25JbnZlbnRvcnkgdiBwYXJyYW1ldHJpIHBlcnNvbmF6YSAodiBzYW1vbSB2ZXJoeSBmYWlsYSlcbiAgICBzdG9yZUludmVudG9yeSgpIHtcbiAgICAgICAgZm9yKGxldCBpbmRleD0wOyBpbmRleDw9dGhpcy5pbnZlbnRvcnlbaW5kZXhdOyBpbmRleCsrKSB7XG4gICAgICAgICAgICBpZih0aGlzLmludmVudG9yeVtpbmRleF0ubmFtZS5pbmNsdWRlcyhcIldlYXBvblwiKSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT2ssIGl0J3MgYSB3ZWFwb25cIik7XG4gICAgICAgICAgICAgICAgdGhpcy53ZWFwb25JbnZlbnRvcnkucHVzaCh0aGlzLmludmVudG9yeVtpbmRleF0pO1xuXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgLy9HZGUgbmFob2RpdHN5YSBvcnl6aWUgKHRoaXMuaW52ZW50b3J5LCB3ZWFwb25JbnZlbnRvcnkpXG4gICAgLy9Pc21hdHJpdmFlbSBvcnl6aWVcbiAgICAvL1Byb3ZlcnlhZW0sIG9yeXppZSBsaSBldG9cbiAgICAvL0JlcmVtIG9yeXppZSBpIGRvYmF2bHlhZW0gZWdvIHYgd2VhcG9uSW52ZW50b3J5XG4gICAgLy9ZZGFseWFlbSBvcnl6aWUgaXogdGhpcy5pbnZlbnRvcnlcbn0iLCIvKmltcG9ydCB7IENoYXJhY3RlciB9IGZyb20gXCIuL2NoYXJhY3RlclwiO1xuXG5leHBvcnQgY2xhc3MgSXRlbSB7XG4gICAgLyoqXG4gICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZVxuICAgICovXG4vKiAgIGNvbnN0cnVjdG9yKG5hbWUpIHtcbiAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgfVxuXG4gICAgLyoqXG4gICAgKiBAcGFyYW0ge0NoYXJhY3Rlcn0gY2hhcmFjdGVyXG4gICAgKi9cbi8qICAgIHVzZUl0ZW0oY2hhcmFjdGVyKSB7XG4gICAgICAgIGNvbnNvbGUubG9nIChcIlVzaW5nIFwiICsgaXRlbS5uYW1lICsgXCIgb24gXCIgKyBjaGFyYWN0ZXIubmFtZSlcbiAgICB9XG59Ki9cblxuaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4vY2hhcmFjdGVyXCI7XG5cbmV4cG9ydCBjbGFzcyBJdGVte1xuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKG5hbWUpe1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuXG4gICAgfVxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSB1dGlsaXNlIGwnaXRlbSBzdXIgdW4gcGVyc29ubmFnZSBkb25uw6lcbiAgICAgKiBAcGFyYW0ge0NoYXJhY3Rlcn0gY2hhcmFjdGVyIExlIHBlcnNvbm5hZ2Ugc3VyIGxlcXVlbCBvbiB1dGlsaXNlIGwnaXRlbVxuICAgICAqL1xuICAgIHVzZU9uKGNoYXJhY3Rlcil7XG4gICAgICAgIC8qIEljaSwgb24gZmFpdCB1bmUgY29uY2F0w6luYXRpb24gZW4gdXRpbGlzYW50IMOgIGxhIGZvaXMgbGVcbiAgICAgICAgbmFtZSBkZSBsJ2l0ZW0gcXUnb24gdXRpbGlzZSAodGhpcy5uYW1lKSBldCBlbiBtw6ptZSB0ZW1wcyBsZSBuYW1lXG4gICAgICAgIGR1IHBlcnNvbm5hZ2Ugc3VyIGxlcXVlbCBvbiB1dGlsaXNlIGwnaXRlbSAoY2hhcmFjdGVyLm5hbWUpXG4gICAgICAgICAqL1xuICAgICAgICBjb25zb2xlLmxvZyhcIlVzaW5nIFwiICsgdGhpcy5uYW1lICsgXCIgb24gXCIgKyBjaGFyYWN0ZXIubmFtZSk7XG4gICAgICAgIFxuICAgIH0gICAgICAgXG59XG5cbiIsIi8qXG4xLiBEYW5zIGxlIHNyYywgY3LDqWVyIHVuIG5vdXZlYXUgZG9zc2llciBleG8sIGRhbnMgY2UgZG9zc2llciBleG8sIFxuY3LDqWVyIHVuIGZpY2hpZXIgY2hhcmFjdGVyLmpzIGF2ZWMgdW5lIGNsYXNzZSBDaGFyYWN0ZXIgw6AgbCdpbnTDqXJpZXVyXG4yLiBGYWlyZSBsYSBtw6l0aG9kZSB0YWtlSXRlbSBxdWkgdmEganVzdGUgcHJlbmRyZSBsJ2FyZ3VtZW50IGl0ZW0gZXQgbCdham91dGVyIGRhbnMgbCdpbnZlbnRhaXJlIGR1IHBlcnNvbm5hZ2UgXG4oYWpvdXRlciB1biB0cnVjIGRhbnMgdW4gdGFibGVhdS4uLi4pXG4ob24gcGV1dCBsYWlzc2VyIGxlIHVzZUl0ZW0gZGUgY8O0dMOpIHBvdXIgbGUgbW9tZW50LCB2dSBxdSdvbiBhIHBhcyBlbmNvcmUgZmFpdCBsYSBjbGFzc2UgSXRlbSlcbjMuIERhbnMgbGUgZG9zc2llciBleG8sIGNyw6llciB1bmUgbm91dmVsbGUgY2xhc3NlIEl0ZW0uIExhIG3DqXRob2RlIHVzZU9uIHBvdXIgbGUgbW9tZW50IHZhIGp1c3RlIFxuZmFpcmUgdW4gY29uc29sZSBsb2cgZGlzYW50IHF1ZSBcIlVzaW5nIFtub20gZGUgbCdpdGVtXSBvbiBbbm9tIGR1IHBlcnNvbm5hZ2VdXCJcbjQuIEZhaXJlIGxhIG3DqXRob2RlIHVzZUl0ZW0gZHUgQ2hhcmFjdGVyIHF1aSB2YSBlbiBncm9zIHByZW5kcmUgbCdpdGVtIMOgIGwnaW5kZXggZG9ubsOpIGRhbnMgXG5zYSBwcm9wcmnDqXTDqSBpbnZlbnRhaXJlIHB1aXMgZMOpY2xlbmNoZXIgbGEgbcOpdGhvZGUgdXNlT24gZGUgbCdJdGVtIHN1ciBsZSBDaGFyYWN0ZXIgbHVpIG3Dqm1lXG41LiBNb2RpZmllciBsYSBtw6l0aG9kZSB0YWtlSXRlbSBwb3VyIGZhaXJlIGVuIHNvcnRlIHF1ZSBsZSBwZXJzb25uYWdlIG5lIHB1aXNzZSBwcmVuZHJlIHF1ZSBkZXMgw6lsw6ltZW50IMOpdGFudCBcbmRlcyBpbnN0YW5jZXMgZGUgaXRlbSBcbjYuIFJham91dGVyIHVuIGlmIGF2ZWMgdW4gaW5zdGFuY2VvZiBwb3VyIHbDqXJpZmllciBzaSBsJ2FyZ3VtZW50IGl0ZW0gZXN0IGJpZW4gdW5lIGluc3RhbmNlIGRlIEl0ZW0gYXZhbnQgZGUgZmFpcmUgbGUgcHVzaFxuKi9cblxuaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4vZXhvL2NoYXJhY3RlclwiO1xuaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuL2V4by9pdGVtXCI7XG5cblxubGV0IHJvbXkgPSBuZXcgQ2hhcmFjdGVyKDEwMCwgJ1JvbXknLCAwLCAwKTtcblxubGV0IGZvcmsgPSBuZXcgSXRlbShcIkZvcmtcIik7XG5sZXQgcGVhciA9IG5ldyBJdGVtKFwiRnJ1aXQ6IHBlYXJcIik7XG5sZXQgYmFuYW5hID0gbmV3IEl0ZW0oXCJGcnVpdDogYmFuYW5hXCIpO1xubGV0IGFwcGxlID0gbmV3IEl0ZW0oXCJGcnVpdDogYXBwbGVcIik7XG5sZXQgb3JhbmdlID0gbmV3IEl0ZW0oXCJGcnVpdDogb3JhbmdlXCIpO1xubGV0IHN3b3JkID0gbmV3IEl0ZW0gKFwiV2VhcG9uOiBzd29yZFwiKTtcblxucm9teS50YWtlSXRlbShmb3JrKTtcbnJvbXkudGFrZUl0ZW0ocGVhcik7XG5yb215LnRha2VJdGVtKGJhbmFuYSk7XG5yb215LnRha2VJdGVtKGFwcGxlKTtcbnJvbXkudGFrZUl0ZW0ob3JhbmdlKTtcbnJvbXkudGFrZUl0ZW0oc3dvcmQpO1xuXG5yb215LnVzZUl0ZW0oMCk7XG5cbnJvbXkuc2hvd0ludmVudG9yeSgpO1xuXG5yb215LmVhdEZydWl0cygpO1xuXG4iXSwic291cmNlUm9vdCI6IiJ9