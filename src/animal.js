/*
1. Créer une classe Food dans son propre fichier
2. La Food aura comme propriété un type en chaîne de caractère (vegetal ou meat ou fish) et un calories en number
3. Faire la méthode eat de Animal pour faire que l'animal réduise sa faim du nombre de calories de la food qu'il mange
4. Faire qu'il ne mange que si le type de food correspond à sa diet (si herbivorous il mange que du vegetal, 
    si omnivorous il mange de tout, si carnivorous il mange que de la meat et du poisson)
 */

/*export class Animal {
    constructor(pawNb, diet) {
        this.isAlive = true;
        this.pawNb = pawNb;
        this.diet = diet;
        this.hunger = 0;
    }
    eat(food) {
        if (food.meat) {
            this.hunger = this.hunger - food.meat;
        } else if (food.fish) {
            this.hunger = this.hunger - food.fish;
        } else {
            this.hunger = this.hunger - food.vegetal;
        }
    }

    play() {

    }
}
*/

import { Food } from "./food";

export class Animal {
    constructor(pawNb, diet, hunger = 0) {
        this.isAlive = true;
        this.pawNb = pawNb;
        this.diet = diet;
        this.hunger = hunger;
    }
    /**
     * @param {Food} food 
     */
    eat(food) {
        
        if (this.diet === 'herbivorous' && food.type === 'vegetal') {
            this.hunger -= food.calories;
        }

        if (this.diet === 'carnivorous' && (food.type === 'meat' || food.type === 'fish')) {
            this.hunger -= food.calories;
        }


        if (this.diet === 'omnivorous') {
            this.hunger -= food.calories;
        }

    }

    play() {
        console.log("I'm playing...")
    }

    toString() {
        return "an animal" //Sledyuchee nuzno ybrat dlya nyznogo rezyltata
    }

    toString() {
        return super.toString()+ " : an animal";
    }
}