/*
I.
1. Dans la classe Dog, faire une méthode bark() qui fera un return de bork bork bork
2. Faire également une méthode fetch(thing:Object) qui fera un console log disant que 
le chien va chercher le truc en concatenant le truc en question dans le console log
3. Faire un nouveau fichier croquette.js qui contiendra une classe Croquette qui héritera 
de la classe Food et qui aura une méthode supplémentaire flip() qui renverse les croquettes 
(un console log) et qui réduit les calories de l'instance actuelle de 20
4. Dans l'index, lancer la méthode fetch pour que le chien aille chercher l'instance 
de food qu'il y a dans banana et voir ce que ça met comme résultat
5. Dans la classe Food, rajouter une méthode toString qui fera un return de "some food" 
puis recharger la page pour voir comment a évolué le fetch

II.
1. Faire en sorte que quand le chien mange des croquettes, il les renverse avant de les manger
(Redéfinir la méthode eat dans la classe Dog)
2. Faire appel à la méthode eat du parent dans la méthode eat du chien 
(il faut pour ça utiliser le super.eat)
3. Dans la méthode eat du chien, vérifier si la food qu'on lui donne c'est des croquettes
(utiliser un if avec un instanceof dedans)
4. Faire que si c'est des croquettes, on déclenche les renverse 
(On declenche la méthode flip de l'argument food)
*/


/*import { Food } from "./food";

export class Croquette extends Food {
    constructor(calories = 20) {

    }

    flip() {
        console.log(Croquette);
        this.calories -= 20;
    }
}
*/

import { Food } from "./food";

export class Croquette extends Food {

    //constructor(calories) {
    // super("meat", calories); - Eto ravnyaetsya otsytsviu constructora
    //}

    flip() {
        console.log("Croquette!!!");
        this.calories -= 20;
        //return this;
    }
}

