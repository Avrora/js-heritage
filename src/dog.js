import { Animal } from "./animal";
import { Croquette } from "./croquette";
import { fileURLToPath } from "url";

export class Dog extends Animal {
    /**
    * @param {string} name
    * @param {string} breed
    */
    constructor(name, breed, hunger = 50) {
        super(4, "omnivourous", hunger);
        this.name = name;
        this.breed = breed;
    }

    bark() {
        return "bork bork bork";
    }

    fetch(thing) { //Ili thing = Object
        console.log('The dog fetch '+thing);
    }

    toString() {
        return super.toString()+ " : a dog";
    }

// Nachala vtoryu chast. Eto vse podskazal Tomas

    eat(food) { //Est to ze, chto i rodirel. Pervie dve strochki - ravnyautsya ih otsytstviu. Kak nichego ne pisali
        super.eat(food);
        if (food instanceof Croquette) {
            food.flip()
        }
    }
}
