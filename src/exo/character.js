/*export class Character {
        /**
    * @param {number} health
    * @param {string} name
    * @param {number} armor
    * @param {number} exp
    * @param {Array<Item>} inventory //Ili Item[]
    */
/*    constructor(health, name, armor, exp, inventory = []) { //V inventorii nyzno dobavit skobki [], inache kod ne srabotaet
        this.health = health;
        this.name = name;
        this.armor = armor;
        this.exp = exp;
        this.inventory = inventory;
    }

    /**
     * @param {Item} item
     */
/*    takeItem(item) {
        this.inventory .push(item); //Tochka pered push, potomy chto eto funkziya
    }
}
*/
import { Item } from "./item";

export class Character {
    /**
     * 
     * @param {number} health 
     * @param {string} name 
     * @param {number} armor 
     * @param {number} exp 
     */
    constructor(health, name, armor, exp) {
        this.health = health;
        this.name = name;
        this.armor = armor;
        this.exp = exp;
        this.inventory = [];
        this.weaponInventory = [];
    }
    /**
     * La méthode takeItem permet à un personnage de "ramasser" un item
     * et de le mettre dans son sac (son inventory)
     * @param {Item} item 
     */
    takeItem(item) {
        //On ne met dans l'inventaire que les instance d'Item et rien d'autre
        if (item instanceof Item) {
            this.inventory.push(item);
        } else {
            console.log("I can only pick items!");
        }

    }
    /**
     * Cette méthode fera qu'un personnage prendra un Item dans son
     * sac puis utilisera l'item sur lui même.
     * @param {number} index l'index de l'item à utiliser
     */
    useItem(index) {
        //On récupère l'item qui est dans l'inventaire à l'index voulu
        let choosedItem = this.inventory[index];
        //On utilise la méthode useOn de l'item en donnant l'instance
        //actuelle de personnage comme cible de l'item
        choosedItem.useOn(this);
    }



    //////////////////////// NACHALA ZDES!!!!
    showInventory() { //kak (this.inventory) 
        /*console.log(character1.inventory);*/
        for (let index=0; index<=this.inventory.length-1; index++) {
            console.log(this.inventory[index].name);
        }
    }

    showOneItem(item){
        
    }

    takeItems(items) { //Eto budet mtablitsa pradmetov array[Item];
        for(let index=0; index<=items.length-1; index++)  {// Zadaem znachenie items - kak let items[] (no tak nelzya s massivom);
            let item = items[index];
            this.inventory.push(item); //ili this.takeItem(item);

        }    
    }

    eatFruits() {
        //fork, pear, banana, apple = inventory = items

       /* console.log(this.health);

        for(let index = 0; index<=this.inventory.length-1; index++) { //Otsuda berem frykti
            if(this.inventory[index].name.indexOf("Fruit")) { //Proverka frykt li eto (vmesto indexOf mojno inludes)
                this.health += 10;
                console.log(this.health);
            }
            else {
                console.log("C'est pas un fruit")
            }
        } */

    //PROVERKI 
        //Otsuda berem frykti
        for(let index = 0; index<this.inventory.length; index++) {

        //Proverka skolko veshei v inventare
        //console.log(this.inventory.length);

            //Proverka frykt li eto (vmesto indexOf mojno inludes)
            if(this.inventory[index].name.includes("Fruit")) {
                console.log(this.inventory[index].name + ". Ok, it's a fruit");
                this.health +=10;
                console.log(this.health);
            } else {
                console.log(this.inventory[index].name +" isn't a fruit");
            }
        }
    }

    eatOneFruit(item){

    }

    //Vnachale nyzno dobavit weaponInventory v parrametri personaza (v samom verhy faila)
    storeInventory() {
        for(let index=0; index<=this.inventory[index]; index++) {
            if(this.inventory[index].name.includes("Weapon")) {
                console.log("Ok, it's a weapon");
                this.weaponInventory.push(this.inventory[index]);

            }
        }
    }
    //Gde nahoditsya oryzie (this.inventory, weaponInventory)
    //Osmatrivaem oryzie
    //Proveryaem, oryzie li eto
    //Berem oryzie i dobavlyaem ego v weaponInventory
    //Ydalyaem oryzie iz this.inventory
}