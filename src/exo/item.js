/*import { Character } from "./character";

export class Item {
    /**
    * @param {string} name
    */
/*   constructor(name) {
    this.name = name;
   }

    /**
    * @param {Character} character
    */
/*    useItem(character) {
        console.log ("Using " + item.name + " on " + character.name)
    }
}*/

import { Character } from "./character";

export class Item{
    /**
     * @param {String} name 
     */
    constructor(name){
        this.name = name;

    }
    /**
     * Méthode qui utilise l'item sur un personnage donné
     * @param {Character} character Le personnage sur lequel on utilise l'item
     */
    useOn(character){
        /* Ici, on fait une concaténation en utilisant à la fois le
        name de l'item qu'on utilise (this.name) et en même temps le name
        du personnage sur lequel on utilise l'item (character.name)
         */
        console.log("Using " + this.name + " on " + character.name);
        
    }       
}

