/*export class Food {
    constructor(meat, fish, vegetal) {
        this.meat = meat;
        this.fish = fish;
        this.vegetal = vegetal;
    }
}
*/

export class Food {
    /**
     * 
     * @param {string} type Types possibles : 'vegetal', 'meat' ou 'fish'
     * @param {number} calories Le nombre de calories de la nourriture
     */
    constructor(type, calories) {
        this.type = type;
        this.calories = calories;
    }

    /*toString() {
        return "Hello, I'm food"; //Dlya alert(banana);
    } 
    */
   
   toString() {
       return "some food";
   }
}
