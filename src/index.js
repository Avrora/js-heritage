/*import "./scss/index.scss";

console.log("this.hunger");


let food = new Food()
food.meat = 2000;
food.fish = 1500;
food.vegetal = 1000;
*/

import  "./scss/index.scss";
import { Food } from "./food";
import { Animal } from "./animal";
import { Dog } from "./dog";
import { Croquette } from "./croquette";

let monkey = new Animal(4, 'omnivorous', 100);

let banana = new Food('vegetal', 75);


monkey.eat(banana);
console.log(monkey.hunger);

let dog = new Dog("Fido", "corgi");
console.log(dog);
dog.play();
dog.eat(banana);
console.log(dog);

console.log(dog instanceof Animal);
console.log([] instanceof Object);

/*
alert(banana); //Dlya toString() 
*/

dog.fetch(banana); 
/*
On ponimaet, otkyada brat dannie, potomy chto zaimstvyet dannie iz Food 
iz-za "let banana = new Food('vegetal', 75)"
Esli bi mi napisali dog.fetch(monkey), nam bi napisali ... object Object,
potomy chto v Animal ne ispolzovan metod toString()
*/

console.log(dog.bark());

dog.fetch(monkey);  //pervii yroven nasledovaliya

dog.fetch(dog); //Vtoroi yroven nasladovaniya

let croquettes = new Croquette("meat", 200);
croquettes.flip();
console.log(croquettes);

dog.eat(croquettes);
croquettes.flip();


