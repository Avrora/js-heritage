/*
1. Dans le src, créer un nouveau dossier exo, dans ce dossier exo, 
créer un fichier character.js avec une classe Character à l'intérieur
2. Faire la méthode takeItem qui va juste prendre l'argument item et l'ajouter dans l'inventaire du personnage 
(ajouter un truc dans un tableau....)
(on peut laisser le useItem de côté pour le moment, vu qu'on a pas encore fait la classe Item)
3. Dans le dossier exo, créer une nouvelle classe Item. La méthode useOn pour le moment va juste 
faire un console log disant que "Using [nom de l'item] on [nom du personnage]"
4. Faire la méthode useItem du Character qui va en gros prendre l'item à l'index donné dans 
sa propriété inventaire puis déclencher la méthode useOn de l'Item sur le Character lui même
5. Modifier la méthode takeItem pour faire en sorte que le personnage ne puisse prendre que des élément étant 
des instances de item 
6. Rajouter un if avec un instanceof pour vérifier si l'argument item est bien une instance de Item avant de faire le push
*/

import { Character } from "./exo/character";
import { Item } from "./exo/item";


let romy = new Character(100, 'Romy', 0, 0);

let fork = new Item("Fork");
let pear = new Item("Fruit: pear");
let banana = new Item("Fruit: banana");
let apple = new Item("Fruit: apple");
let orange = new Item("Fruit: orange");
let sword = new Item ("Weapon: sword");

romy.takeItem(fork);
romy.takeItem(pear);
romy.takeItem(banana);
romy.takeItem(apple);
romy.takeItem(orange);
romy.takeItem(sword);

romy.useItem(0);

romy.showInventory();

romy.eatFruits();

