module.exports = {
    mode: 'development',
    entry: {
        main: './src/index.js',
        exercise: './src/rpg.js' //Nazvanie "exo-rpg" (bilo ranche) v kavichkah, potomy chto est defis
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                "sass-loader" // compiles Sass to CSS, using Node Sass by default
            ]
        }]
    },
    devtool: 'inline-source-map'
};
